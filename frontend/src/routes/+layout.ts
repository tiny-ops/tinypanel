import type { LayoutLoad } from './$types';
import { AppConfig } from '$lib';

export const load: LayoutLoad = async () => {
	return {
		config: new AppConfig()
	};
};