import type { User } from '$lib';
import { UserStatus } from '$lib';

export enum UsersViewFilter {
	Enabled,
	Disabled,
	All
}

export function getUsersWithViewFilter(users: User[], 
                                       filter: UsersViewFilter,
                                       searchMask: string): User[] {

  const mask = searchMask.toLocaleLowerCase();

  if (filter === UsersViewFilter.Enabled) {

    return users.filter((user) => {
      if (searchMask.length > 0) {
        return user.status === UserStatus.Enabled && user.name.toLocaleLowerCase().includes(mask)

      } else {
        return user.status === UserStatus.Enabled
      }
    })
    
  } else if (filter === UsersViewFilter.Disabled) {
    return users.filter((user) => {
      if (searchMask.length > 0) {
        return user.status === UserStatus.Disabled && user.name.toLocaleLowerCase().includes(mask)

      } else {
        return user.status === UserStatus.Disabled
      }
    })
    
  } else {
    return users
  }
}