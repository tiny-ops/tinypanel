export class AppConfig {
  activeClients: OpenVpnClient[] = [];
  users: User[] = [];

  openvpnConfig: OpenVpnConfig = new OpenVpnConfig();

  ovpnTemplate: String = '';

  openvpnServiceStatus: OpenVpnServiceStatus = OpenVpnServiceStatus.Stopped
}

export class OpenVpnClient {
  name: string = '';
  realAddress: string = '';
  bytesReceived: number = 0;
  bytesSent: number = 0;

  connectedSince: string = new Date().toString();

  virtualAddress: string = '';
}

export class User {
  id: number = 0;
  name: string = '';
  fullName: string = '';
  status: UserStatus = UserStatus.Disabled
}

export enum UserStatus {
  Enabled = 'Enabled', Disabled = 'Disabled'
}

export class OpenVpnConfig {
  publicHost: string = '';
  publicPort: number = 0;
  serviceName: string = '';
  protocolType: string = '';
  easyRsaPath: string = '';
  tlsAuthKeyPath: string = '';
  statusFilePath: string = '';
}

export enum OpenVpnServiceStatus {
  Running = 'Running', Stopped = 'Stopped'
}