import { toast } from '@zerodevx/svelte-toast';

export const showInfo = (message: string) => toast.push(message, { theme: {} });

export const showError = (message: string) => toast.push(message, {
  duration: 10000,
  // theme: {
  //   '--toastBackground': '#e30000',
  //   '--toastColor': 'white',
  // }
});