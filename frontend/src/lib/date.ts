export function getDateInHumanFormat(input: string): string {
  const date = new Date(input);

  const hour = addLeadingZeroes(date.getHours());
  const day = addLeadingZeroes(date.getDate());
  const month = addLeadingZeroes(date.getMonth());
  const seconds = addLeadingZeroes(date.getSeconds());

  return `${day}.${month}.${date.getFullYear()} ${hour}:${month}:${seconds}`
}

function addLeadingZeroes(input: number): string {
  if (input.toString().length === 1) {
    return `0${input}`

  } else {
    return input.toString()
  }
}