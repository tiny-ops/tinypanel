export class CreateUserRequest {
  constructor(name: string, fullName: string) {
    this.name = name;
    this.fullName = fullName;
  }

  name: string;
  fullName: string;
}