export function getPrettySize(value: number): string {
  let result;

  if (value <= 1000) {
    result = Math.round(value) + ' B';

  } else if ((value < 1000000) && (value > 999)) {
    result = Math.round(value / 1000) + ' KB';

  } else if ((value < 1000000000) && (value > 999999)) {
    result = Math.round(value / 1000 / 1000) + ' MB';

  } else if (value >= 1000000000) {
    result = Math.round(value / 1000 / 1000 / 1000) + ' GB';

  } else {
    result = Math.round(value) + ' KB';
  }

  return result;
}