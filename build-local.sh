#!/bin/bash

cd frontend
npm run build

cd ..
rm -rf static && mkdir static
cp -r frontend/build/. static/

cross build --release

cp target/x86_64-unknown-linux-gnu/release/tinypanel .

eu-elfcompress tinypanel
strip tinypanel
upx -9 --lzma tinypanel

echo "---"
echo "complete"