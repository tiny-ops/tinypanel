# Installation

```shell
openvpn ALL=NOPASSWD: /usr/bin/systemctl start openvpn@server
openvpn ALL=NOPASSWD: /usr/bin/systemctl stop openvpn@server
openvpn ALL=NOPASSWD: /usr/bin/systemctl status openvpn@server
```