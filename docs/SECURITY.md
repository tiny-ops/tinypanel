# Security

Current version doesn't provide authentication mechanisms, so you can
use basic auth to restricting access.

## Basic auth setup

Install `httpd-tools` or use openssl.

Create user:

```shell
htpasswd -c /etc/nginx/.htpasswd some-user

# with openssl
printf "some-user:$(openssl passwd -apr1 P@55w0rd)\n" > /etc/nginx/.htpasswd
```

Add two directives to `location` section:

```nginx
location / {
    ...
    auth_basic "Restricted Content";
    auth_basic_user_file /etc/nginx/.htpasswd;
    ...
}
```

Reload nginx configuration

```shell
systemctl reload nginx
```