# API

## Get backend config

- URL: `/api/config`
- Method: `GET`

## User management

### Create user

- URL: `/api/user`
- Method: `POST`

Body:

```json
{
  "name": "johnny",
  "full_name": "Johnny"
}
```

### Disable user

- URL: `/api/user/{name}/disable`
- Method: `POST`

### Import users from easy-rsa registry

- URL: `/api/user/import`
- Method: `POST`

### Get OVPN config for user

- URL: `/api/user/{name}/ovpn`
- Method: 'GET'

## Manage OpenVPN service

### Start service

- URL: `/api/service/start`
- Method: `POST`

### Stop service

- URL: `/api/service/stop`
- Method: `POST`

### Get service status

- URL: `/api/service/status`
- Method: `GET`

Returns struct:

```json
{
  "status": "running"
}
```

Possible values: `running` or `stopped`