# Development

## Create new certificates without confirmation

Easyrsa script asks for `yes` when you create a new certificate.

Comment out this chunks of code:

```bash
confirm "Confirm request details: " "yes" "\
You are about to sign the following certificate:
${foriegn_request}Request subject, to be signed as a \
$crt_type certificate ${valid_period}:

$(display_dn req "$req_in")" # => confirm end
```

and

```bash
	confirm "  Continue with revocation: " "yes" "
Please confirm that you wish to revoke the certificate
with the following subject:

  $(display_dn x509 "$crt_in")

  serial-number: $cert_serial

  Reason: ${crl_reason:-None given}"
```

Prepare PKI and build CA:

```shell
./easyrsa init-pki
./easyrsa build-ca nopass
```

## How to build

```shell
cd frontend
npm i
npm run build
cd ..
```

Copy artifact from `frontend/build` to `static`:

```shell
mkdir static
cargo install cross
cross build --release
```