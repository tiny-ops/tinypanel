use std::net::TcpListener;
use std::path::Path;

use actix_cors::Cors;
use actix_plus_static_files::{build_hashmap_from_included_dir, Dir, include_dir, ResourceFiles};
use actix_web::{HttpServer, web};
use actix_web::dev::Server;
use log::info;
use sqlx::{Pool, Sqlite};

use crate::config::AppConfig;
use crate::logging::logging::get_logging_config;
use crate::openvpn::status::parser::{get_status_file_format, OpenVpnStatusFileParser};
use crate::routes::config::{get_config_route, rebuild_ovpn_configs_route};
use crate::routes::import::import_users_from_easyrsa_registry_route;
use crate::routes::service::{openvpn_service_management_route, openvpn_service_status_route};
use crate::routes::user::{create_user_route, delete_user_route, disable_user_route, enable_user_route};
use crate::routes::version_route;
use crate::state::{connect_to_db, create_db_tables};
use crate::types::OperationResult;

const STATIC_DIR: Dir = include_dir!("./static");

pub struct Application {
    port: u16,
    server: Server,
}

impl Application {
    pub fn get_version() -> String {
        "1.0.0".to_string()
    }

    pub fn port(&self) -> u16 {
        self.port
    }

    pub async fn build(config: AppConfig) -> Result<Self, anyhow::Error> {
        let logging_config = get_logging_config(&config.log_level);

        match log4rs::init_config(logging_config) {
            Ok(_) => {}
            Err(e) => eprintln!("{}", e)
        }

        info!("config: {:?}", config);

        let status_file_format = get_status_file_format(&config.openvpn.status_file_path.to_string())?;

        let openvpn_status_file_parser = OpenVpnStatusFileParser::new(status_file_format);

        let db_pool = connect_to_db(&config.db_cnn).await?;
        create_db_tables(&db_pool).await?;

        let address = format!("localhost:{}", config.port);
        let listener = TcpListener::bind(&address)?;
        let port = listener.local_addr().expect("unable to get socket").port();

        let server = run(config.clone(), db_pool, openvpn_status_file_parser, listener).await?;

        Ok(Self { port, server })
    }

    pub async fn run_until_stopped(self) -> Result<(), std::io::Error> {
        self.server.await
    }
}

pub async fn run(config: AppConfig, db_pool: Pool<Sqlite>,
                 openvpn_status_file_parser: OpenVpnStatusFileParser,
                 listener: TcpListener) -> OperationResult<Server> {

    let app_banner = format!("TINY PANEL v{}", Application::get_version());
    println!("{}", app_banner);
    info!("{}", app_banner);

    let hash_map = build_hashmap_from_included_dir(&STATIC_DIR);

    let server = HttpServer::new(move || {
        let cors = Cors::default()
            .allow_any_header()
            .allow_any_origin()
            .supports_credentials()
            .allowed_methods(vec!["GET", "POST", "PUT", "DELETE"])
            .max_age(3600);

        actix_web::App::new()
            .wrap(cors)
            .app_data(web::Data::new(config.clone()))
            .app_data(web::Data::new(db_pool.clone()))
            .app_data(web::Data::new(openvpn_status_file_parser.clone()))
            .service(version_route)
            .service(get_config_route)
            .service(create_user_route)
            .service(disable_user_route)
            .service(delete_user_route)
            .service(enable_user_route)
            .service(create_user_route)
            .service(openvpn_service_status_route)
            .service(openvpn_service_management_route)
            .service(import_users_from_easyrsa_registry_route)
            .service(rebuild_ovpn_configs_route)
            .service(actix_files::Files::new("/api/files/ovpn",
                                             Path::new("ovpn")).show_files_listing())
            .service(ResourceFiles::new(
                "/", hash_map.clone()).resolve_not_found_to_root()
            )
    }).listen(listener)?
        .run();

    Ok(server)
}