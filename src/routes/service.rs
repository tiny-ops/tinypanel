use actix_web::{get, HttpResponse, post, Responder, web};
use log::error;

use crate::config::AppConfig;
use crate::openvpn::service::lifecycle::{get_openvpn_service_status, start_openvpn_service, stop_openvpn_service};
use crate::routes::dto::service::OpenVpnServiceStatusDto;

#[get("/api/service/status")]
pub async fn openvpn_service_status_route(config: web::Data<AppConfig>) -> impl Responder {
    match get_openvpn_service_status(&config.openvpn.service_name) {
        Ok(status) => {
            HttpResponse::Ok().json(
                OpenVpnServiceStatusDto {
                    status
                }
            )
        }
        Err(e) => {
            error!("{}", e);
            HttpResponse::InternalServerError().finish()
        }
    }
}

#[post("/api/service/{command}")]
pub async fn openvpn_service_management_route(
    command: web::Path<String>,
    config: web::Data<AppConfig>) -> impl Responder {

    let command_name = command.into_inner();

    match command_name.as_str() {
        "start" => {
            match start_openvpn_service(&config.openvpn.service_name) {
                Ok(_) => HttpResponse::Ok().finish(),
                Err(e) => {
                    error!("{}", e);
                    HttpResponse::InternalServerError().finish()
                }
            }
        }
        "stop" => {
            match stop_openvpn_service(&config.openvpn.service_name) {
                Ok(_) => HttpResponse::Ok().finish(),
                Err(e) => {
                    error!("{}", e);
                    HttpResponse::InternalServerError().finish()
                }
            }
        }
        _ => {
            error!("unsupported service command '{command_name}'");
            HttpResponse::BadRequest().finish()
        }
    }
}