use actix_web::{get, HttpResponse, post, Responder, web};
use log::{error, info};
use sqlx::{Pool, Sqlite};

use crate::config::AppConfig;
use crate::openvpn::client::ovpn::rebuild_ovpn_configs;
use crate::openvpn::status::parser::OpenVpnStatusFileParser;
use crate::routes::dto::config::get_app_config_dto;

#[get("/api/config")]
pub async fn get_config_route(config: web::Data<AppConfig>,
                              db_pool: web::Data<Pool<Sqlite>>,
                      status_file_parser: web::Data<OpenVpnStatusFileParser>) -> impl Responder {
    info!("get app config route");

    match get_app_config_dto(&db_pool, &config, &status_file_parser).await {
        Ok(app_config) => HttpResponse::Ok().json(app_config),
        Err(e) => {
            error!("{}", e);
            HttpResponse::InternalServerError().finish()
        }
    }
}

#[post("/api/config/ovpn/rebuild")]
pub async fn rebuild_ovpn_configs_route(config: web::Data<AppConfig>,
                                        db_pool: web::Data<Pool<Sqlite>>) -> impl Responder {
    match rebuild_ovpn_configs(&db_pool, &config.openvpn).await {
        Ok(app_config) => HttpResponse::Ok().json(app_config),
        Err(e) => {
            error!("{}", e);
            HttpResponse::InternalServerError().finish()
        }
    }
}