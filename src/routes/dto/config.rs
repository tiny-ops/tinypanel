use anyhow::Context;
use serde::Serialize;
use sqlx::{Pool, Sqlite};
use tokio::fs;

use crate::config::{AppConfig, OpenVpnConfig};
use crate::openvpn::service::lifecycle::{get_openvpn_service_status, OpenVpnServiceStatus};
use crate::openvpn::status::OpenVpnClient;
use crate::openvpn::status::parser::OpenVpnStatusFileParser;
use crate::openvpn::user::User;
use crate::state::user::find::find_users;
use crate::types::OperationResult;

#[derive(Serialize, Clone, PartialEq, Debug)]
#[serde(rename_all = "camelCase")]
pub struct AppConfigDto {
    pub active_clients: Vec<OpenVpnClient>,

    pub users: Vec<User>,

    pub openvpn_config: OpenVpnConfig,

    pub ovpn_template: String,

    pub openvpn_service_status: OpenVpnServiceStatus
}

pub async fn get_app_config_dto(db_pool: &Pool<Sqlite>, config: &AppConfig,
                    status_file_parser: &OpenVpnStatusFileParser) -> OperationResult<AppConfigDto> {
    let users = find_users(&db_pool).await?;

    let active_clients_status =
        status_file_parser.get_status(&config.openvpn.status_file_path)
            .context("read ovpn service status file error")?;

    let openvpn_service_status = get_openvpn_service_status(&config.openvpn.service_name)
        .unwrap_or(OpenVpnServiceStatus::Stopped);

    let ovpn_template = fs::read_to_string("ovpn.template").await.context("unable to read ovpn.template")?;

    let app_config = AppConfigDto {
        active_clients: active_clients_status.clients.clone(),
        users,
        openvpn_config: config.openvpn.clone(),
        ovpn_template,
        openvpn_service_status,
    };

    Ok(app_config)
}