use serde::Serialize;

use crate::openvpn::service::lifecycle::OpenVpnServiceStatus;

#[derive(Serialize,Debug,Clone)]
pub struct OpenVpnServiceStatusDto {
    pub status: OpenVpnServiceStatus
}