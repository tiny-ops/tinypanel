use non_blank_string_rs::NonBlankString;
use serde::Deserialize;

#[derive(Deserialize,Debug)]
#[serde(rename_all = "camelCase")]
pub struct CreateUserRequest {
    pub name: NonBlankString,
    pub full_name: NonBlankString
}