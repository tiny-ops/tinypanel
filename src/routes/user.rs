use actix_web::{delete, HttpResponse, post, Responder, web};
use log::error;
use sqlx::{Pool, Sqlite};

use crate::config::AppConfig;
use crate::openvpn::user::create::create_openvpn_user;
use crate::openvpn::user::delete::delete_openvpn_user;
use crate::openvpn::user::disable::disable_openvpn_user;
use crate::openvpn::user::enable::enable_openvpn_user;
use crate::routes::dto::user::CreateUserRequest;

#[post("/api/user")]
pub async fn create_user_route(
    app_config: web::Data<AppConfig>,
    db_pool: web::Data<Pool<Sqlite>>,
    request: web::Json<CreateUserRequest>) -> impl Responder {

    match create_openvpn_user(&db_pool, &app_config.openvpn,
                              &request.name, &request.full_name).await {
        Ok(_) => HttpResponse::Ok().finish(),
        Err(e) => {
            error!("unable to create openvpn user: {}", e);
            HttpResponse::InternalServerError().finish()
        }
    }
}

#[post("/api/user/{name}/disable")]
pub async fn disable_user_route(
    params: web::Path<(String, )>,
    app_config: web::Data<AppConfig>,
    db_pool: web::Data<Pool<Sqlite>>) -> impl Responder {

    let name = params.into_inner().0;

    match disable_openvpn_user(&db_pool, &app_config.openvpn.easy_rsa_path, &name).await {
        Ok(_) => HttpResponse::Ok().finish(),
        Err(e) => {
            error!("unable to disable openvpn user: {}", e);
            HttpResponse::InternalServerError().finish()
        }
    }
}

#[post("/api/user/{name}/enable")]
pub async fn enable_user_route(
    params: web::Path<(String, )>,
    app_config: web::Data<AppConfig>,
    db_pool: web::Data<Pool<Sqlite>>) -> impl Responder {

    let name = params.into_inner().0;

    match enable_openvpn_user(&db_pool, &app_config.openvpn.easy_rsa_path, &name).await {
        Ok(_) => HttpResponse::Ok().finish(),
        Err(e) => {
            error!("unable to re-enable openvpn user: {}", e);
            HttpResponse::InternalServerError().finish()
        }
    }
}

#[delete("/api/user/{name}")]
pub async fn delete_user_route(
    params: web::Path<(String, )>,
    app_config: web::Data<AppConfig>,
    db_pool: web::Data<Pool<Sqlite>>) -> impl Responder {

    let name = params.into_inner().0;

    match delete_openvpn_user(&db_pool, &app_config.openvpn.easy_rsa_path, &name).await {
        Ok(_) => HttpResponse::Ok().finish(),
        Err(e) => {
            error!("unable to delete openvpn user: {}", e);
            HttpResponse::InternalServerError().finish()
        }
    }
}