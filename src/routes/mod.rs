use actix_web::{get, HttpResponse, Responder};

use crate::startup::Application;

pub mod service;
pub mod dto;

pub mod config;
pub mod user;
pub mod import;

#[get("/api/version")]
pub async fn version_route() -> impl Responder {
    HttpResponse::Ok().body(Application::get_version())
}