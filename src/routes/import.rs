use actix_web::{HttpResponse, post, Responder, web};
use log::error;
use sqlx::{Pool, Sqlite};

use crate::config::AppConfig;
use crate::openvpn::user::import::import_users_from_easy_rsa_registry;

#[post("/api/user/import")]
pub async fn import_users_from_easyrsa_registry_route(
    app_config: web::Data<AppConfig>,
    db_pool: web::Data<Pool<Sqlite>>) -> impl Responder {

    match import_users_from_easy_rsa_registry(&db_pool, &app_config.openvpn).await {
        Ok(_) => HttpResponse::Ok().finish(),
        Err(e) => {
            error!("unable to import users from certs: {}", e);
            HttpResponse::InternalServerError().finish()
        }
    }
}