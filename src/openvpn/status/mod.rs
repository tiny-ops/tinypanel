use chrono::NaiveDateTime;
use serde::Serialize;

pub mod file;
pub mod parser;

#[derive(PartialEq,Clone,Debug)]
pub struct OpenVpnClientStatus {
    pub openvpn_version: Option<String>,

    /// Unixtime
    pub updated: NaiveDateTime,
    pub clients: Vec<OpenVpnClient>
}

#[derive(Serialize,PartialEq,Clone,Debug)]
#[serde(rename_all = "camelCase")]
pub struct OpenVpnClient {
    pub name: String,
    pub real_address: String,
    pub bytes_received: u64,
    pub bytes_sent: u64,

    /// Unixtime
    pub connected_since: NaiveDateTime,
    pub last_ref: NaiveDateTime,

    pub virtual_address: String
}