use std::fs;

use anyhow::anyhow;
use chrono::{Local, NaiveDateTime};
use log::{debug, error, info};

use crate::openvpn::status::{OpenVpnClient, OpenVpnClientStatus};
use crate::types::OperationResult;

pub fn get_clients_status_from_status_file(file_path: &str) -> OperationResult<OpenVpnClientStatus> {
    info!("getting client list status from file '{file_path}'..");

    let mut file_header_reached = false;

    let mut first_table_header_reached = false;
    let mut first_table_end_reached = false;
    let mut routing_table_header_reached = false;
    let mut routing_table_end_reached = false;

    let content = fs::read_to_string(file_path)?;

    let lines = content.split("\n").collect::<Vec<&str>>();

    let mut status_updated: NaiveDateTime = Local::now().naive_local();

    let mut clients: Vec<OpenVpnClient> = vec![];
    let mut results: Vec<OpenVpnClient> = vec![];

    for line in lines {
        if !first_table_end_reached && line.starts_with("ROUTING") {
            debug!("first client table has been parsed");
            first_table_end_reached = true;
        }

        if !routing_table_end_reached && line.starts_with("GLOBAL") {
            debug!("routing client table has been parsed");
            routing_table_end_reached = true;
        }

        if !first_table_header_reached && file_header_reached {
            let updated_row_parts = line.split(",").collect::<Vec<&str>>();

            if updated_row_parts.len() == 2 {
                let updated_part = updated_row_parts.last().unwrap();
                status_updated = parse_datetime_from_status_string(updated_part)?;
            }
        }

        if first_table_header_reached && !first_table_end_reached {
            let client = get_client_status_from_table_row(&line)?;
            clients.push(client);
        }

        if routing_table_header_reached && !routing_table_end_reached {
            let client = get_client_status_from_table_row(&line)?;

            if let Some(existing_client) = clients.iter()
                                                .find(|c| c.name == client.name) {

                let updated_client = OpenVpnClient {
                    name: existing_client.name.to_string(),
                    real_address: existing_client.real_address.to_string(),
                    bytes_received: existing_client.bytes_received,
                    bytes_sent: existing_client.bytes_sent,
                    connected_since: existing_client.connected_since,
                    virtual_address: client.virtual_address.to_string(),
                    last_ref: client.last_ref
                };

                debug!("client: {:?}", updated_client);
                results.push(updated_client);
            }
        }

        if !file_header_reached && line.starts_with("OpenVPN") {
            file_header_reached = true;
        }

        if !first_table_header_reached && line.starts_with("Common Name") {
            debug!("status updated value has been parsed");
            first_table_header_reached = true;
        }

        if !routing_table_header_reached && line.starts_with("Virtual Address") {
            routing_table_header_reached = true;
        }
    }

    Ok(
        OpenVpnClientStatus {
            openvpn_version: None,
            updated: status_updated,
            clients: results,
        }
    )
}

fn get_client_status_from_table_row(row: &str) -> OperationResult<OpenVpnClient> {
    debug!("get client status from row '{row}'");
    let parts = row.split(",").collect::<Vec<&str>>();

    if parts.len() == 5 {
        Ok(
            OpenVpnClient {
                name: parts.first().unwrap().to_string(),
                real_address: parts.get(1).unwrap().to_string(),
                bytes_received: parts.get(2).unwrap().parse()?,
                bytes_sent: parts.get(3).unwrap().parse()?,
                connected_since: parse_datetime_from_status_string(
                    &parts.last().unwrap().to_string())?,
                virtual_address: "".to_string(),
                last_ref: Local::now().naive_local()
            }
        )

    } else if parts.len() == 4 {
        Ok(
            OpenVpnClient {
                name: parts.get(1).unwrap().to_string(),
                real_address: parts.get(2).unwrap().to_string(),
                bytes_received: 0,
                bytes_sent: 0,
                last_ref: parse_datetime_from_status_string(
                    &parts.last().unwrap().to_string())?,
                virtual_address: parts.first().unwrap().to_string(),
                connected_since: Local::now().naive_local()
            }
        )

    } else {
        Err(anyhow!("unsupported client status format"))
    }
}

fn parse_datetime_from_status_string(datetime_str: &str) -> OperationResult<NaiveDateTime> {
    match NaiveDateTime::parse_from_str(datetime_str, "%a %b %e %T %Y") {
        Ok(value) => Ok(value),
        Err(e) => {
            error!("unable to parse date: {}", e);
            info!("another attempt with different format..");

            let value = NaiveDateTime::parse_from_str(datetime_str, "%Y-%m-%d %H:%M:%S")?;

            Ok(value)
        }
    }
}

#[cfg(test)]
mod tests {
    use std::path::Path;

    use chrono::{Datelike, Timelike};

    use crate::openvpn::status::file::{get_client_status_from_table_row, get_clients_status_from_status_file, parse_datetime_from_status_string};
    use crate::openvpn::status::OpenVpnClient;
    use crate::tests::init_logging;

    #[test]
    fn client_status_row_should_be_parsed() {
        let result = get_client_status_from_table_row("e.hlopkova,157.66.159.277:49212,378283,910367,Fri Jul 24 19:02:57 2020").unwrap();

        assert_eq!(result.name, "e.hlopkova");
        assert_eq!(result.real_address, "157.66.159.277:49212");
        assert_eq!(result.bytes_received, 378283);
        assert_eq!(result.bytes_sent, 910367);
        assert_eq!(result.connected_since, parse_datetime_from_status_string("Fri Jul 24 19:02:57 2020").unwrap());
        assert_eq!(result.virtual_address, "");

        //

        let result = get_client_status_from_table_row("10.2.0.3,e.hlopkova,157.66.159.277:49212,Fri Jul 24 19:16:18 2020").unwrap();

        assert_eq!(result.name, "e.hlopkova");
        assert_eq!(result.real_address, "157.66.159.277:49212");
        assert_eq!(result.bytes_received, 0);
        assert_eq!(result.bytes_sent, 0);
        assert_eq!(result.last_ref, parse_datetime_from_status_string("Fri Jul 24 19:16:18 2020").unwrap());
        assert_eq!(result.virtual_address, "10.2.0.3");
    }

    #[test]
    fn openvpn_date_format_should_be_parsed() {
        let result = parse_datetime_from_status_string("Fri Jul 24 09:25:02 2020").unwrap();

        assert_eq!(2020, result.year());
        assert_eq!(7, result.month());
        assert_eq!(24, result.day());
        assert_eq!(9, result.hour());
        assert_eq!(25, result.minute());
        assert_eq!(2, result.second());
    }

    #[test]
    fn client_status_should_be_loaded_from_old_format() {
        init_logging();

        let file_path = Path::new("test-data").join("openvpn-status.log");
        let file_path = format!("{}", file_path.display());

        let status = get_clients_status_from_status_file(&file_path).unwrap();

        assert_eq!(5, status.clients.len());

        let updated_value = status.updated.format("%Y-%m-%d %H:%M:%S").to_string();

        assert_eq!("2020-07-24 19:16:23", updated_value);

        let first_client = status.clients.iter().find(|c| c.name == "e.hlopkova").unwrap();

        let expected_first_client = get_expected_first_client();

        assert_eq!(expected_first_client, first_client.clone());
    }

    #[test]
    fn empty_file_test() {
        init_logging();

        let file_path = Path::new("test-data").join("openvpn-status-empty.log");
        let file_path = format!("{}", file_path.display());

        let status = get_clients_status_from_status_file(&file_path).unwrap();

        assert!(status.clients.is_empty());

        assert_eq!(5, status.updated.day());
        assert_eq!(10, status.updated.month());
        assert_eq!(2023, status.updated.year());
    }

    #[test]
    fn return_error_for_unknown_file() {
        assert!(get_clients_status_from_status_file("unknown-file").is_err())
    }

    fn get_expected_first_client() -> OpenVpnClient {
        OpenVpnClient {
            name: "e.hlopkova".to_string(),
            real_address: "157.66.159.277:49212".to_string(),
            bytes_received: 378283,
            bytes_sent: 910367,
            connected_since: parse_datetime_from_status_string("Fri Jul 24 19:02:57 2020").unwrap(),
            virtual_address: "10.2.0.3".to_string(),
            last_ref: parse_datetime_from_status_string("Fri Jul 24 19:16:18 2020").unwrap(),
        }
    }
}