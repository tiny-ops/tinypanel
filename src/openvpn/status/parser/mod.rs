use std::fs;

use anyhow::anyhow;
use chrono::{Local, NaiveDateTime};
use log::{debug, error, info};

use crate::openvpn::status::{OpenVpnClient, OpenVpnClientStatus};
use crate::types::OperationResult;

const DEFAULT_DATE_FORMAT: &str = "%a %b  %e %T %Y";
const LEGACY_DATE_FORMAT: &str = "%a %b %e %T %Y";

#[derive(Clone)]
pub struct OpenVpnStatusFileParser {
    pub format: StatusFileFormat
}

impl OpenVpnStatusFileParser {
    pub fn new(format: StatusFileFormat) -> OpenVpnStatusFileParser {
        OpenVpnStatusFileParser {
            format
        }
    }

    pub fn get_status(&self, status_file_path: &str) -> OperationResult<OpenVpnClientStatus> {
        let content = fs::read_to_string(status_file_path)?;

        let status = match self.format {
            StatusFileFormat::Default => self.get_status_from_content(&content),
            _ => self.get_status_from_content_in_legacy_format(&content)
        }?;

        Ok(status)
    }

    fn get_status_from_content(&self, content: &str) -> OperationResult<OpenVpnClientStatus> {
        let rows = content.split("\n").collect::<Vec<&str>>();

        if rows.len() >= 6 {
            let mut clients: Vec<OpenVpnClient> = vec![];

            let mut openvpn_version: Option<String> = None;

            let mut updated: NaiveDateTime = NaiveDateTime::default();

            for row in rows.iter() {
                if row.starts_with("TITLE,") {
                    let row_parts = row.split(" ").collect::<Vec<&str>>();

                    if row_parts.len() == 16 {
                        openvpn_version = Some(row_parts.get(1).unwrap().to_string());
                    }

                    continue
                }

                if row.starts_with("TIME,") {
                    let row_parts = row.split(",").collect::<Vec<&str>>();

                    if row_parts.len() == 3 {
                        let updated_str = row_parts.get(1).unwrap().to_string();
                        updated = NaiveDateTime::parse_from_str(&updated_str, DEFAULT_DATE_FORMAT)?;
                    }

                    continue
                }

                if row.starts_with("CLIENT_LIST,") {
                    let client = self.get_client_from_client_list_row(&row)?;
                    clients.push(client);
                }
            }

            Ok(
                OpenVpnClientStatus {
                    openvpn_version,
                    updated,
                    clients,
                }
            )

        } else {
            error!("unsupported syntax of status file, rows expected at least: 6");
            Err(anyhow!("unsupported syntax of status file"))
        }
    }

    fn get_client_from_client_list_row(&self, row: &str) -> OperationResult<OpenVpnClient> {
        let parts = row.split(",").collect::<Vec<&str>>();

        if parts.len() == 12 {
            let connected_since = NaiveDateTime::parse_from_str(
                parts.get(7).unwrap(), DEFAULT_DATE_FORMAT)?;

            Ok(
                OpenVpnClient {
                    name: parts.get(1).unwrap().to_string(),
                    real_address: parts.get(2).unwrap().to_string(),
                    bytes_received: parts.get(5).unwrap().parse::<u64>().unwrap(),
                    bytes_sent: parts.get(6).unwrap().parse::<u64>().unwrap(),
                    connected_since,
                    last_ref: Default::default(),
                    virtual_address: parts.get(3).unwrap().to_string(),
                }
            )

        } else {
            error!("unsupported client list row format, expected 12 parts");
            Err(anyhow!("unsupported format"))
        }
    }

    fn get_status_from_content_in_legacy_format(&self, content: &str) -> OperationResult<OpenVpnClientStatus> {
        let mut file_header_reached = false;

        let mut first_table_header_reached = false;
        let mut first_table_end_reached = false;
        let mut routing_table_header_reached = false;
        let mut routing_table_end_reached = false;

        let lines = content.split("\n").collect::<Vec<&str>>();

        let mut status_updated: NaiveDateTime = Local::now().naive_local();

        let mut clients: Vec<OpenVpnClient> = vec![];
        let mut results: Vec<OpenVpnClient> = vec![];

        for line in lines {
            if !first_table_end_reached && line.starts_with("ROUTING") {
                debug!("first client table has been parsed");
                first_table_end_reached = true;
            }

            if !routing_table_end_reached && line.starts_with("GLOBAL") {
                debug!("routing client table has been parsed");
                routing_table_end_reached = true;
            }

            if !first_table_header_reached && file_header_reached {
                let updated_row_parts = line.split(",").collect::<Vec<&str>>();

                if updated_row_parts.len() == 2 {
                    let updated_part = updated_row_parts.last().unwrap();
                    status_updated = self.parse_datetime_from_status_string(updated_part)?;
                }
            }

            if first_table_header_reached && !first_table_end_reached {
                let client = self.get_client_status_from_table_row(&line)?;
                clients.push(client);
            }

            if routing_table_header_reached && !routing_table_end_reached {
                let client = self.get_client_status_from_table_row(&line)?;

                if let Some(existing_client) = clients.iter()
                    .find(|c| c.name == client.name) {

                    let updated_client = OpenVpnClient {
                        name: existing_client.name.to_string(),
                        real_address: existing_client.real_address.to_string(),
                        bytes_received: existing_client.bytes_received,
                        bytes_sent: existing_client.bytes_sent,
                        connected_since: existing_client.connected_since,
                        virtual_address: client.virtual_address.to_string(),
                        last_ref: client.last_ref
                    };

                    debug!("client: {:?}", updated_client);
                    results.push(updated_client);
                }
            }

            if !file_header_reached && line.starts_with("OpenVPN") {
                file_header_reached = true;
            }

            if !first_table_header_reached && line.starts_with("Common Name") {
                debug!("status updated value has been parsed");
                first_table_header_reached = true;
            }

            if !routing_table_header_reached && line.starts_with("Virtual Address") {
                routing_table_header_reached = true;
            }
        }

        Ok(
            OpenVpnClientStatus {
                openvpn_version: None,
                updated: status_updated,
                clients: results,
            }
        )
    }

    fn parse_datetime_from_status_string(&self, datetime_str: &str) -> OperationResult<NaiveDateTime> {
        match NaiveDateTime::parse_from_str(datetime_str, LEGACY_DATE_FORMAT) {
            Ok(value) => Ok(value),
            Err(e) => {
                error!("unable to parse date: {}", e);
                info!("another attempt with different format..");

                let value = NaiveDateTime::parse_from_str(datetime_str, "%Y-%m-%d %H:%M:%S")?;

                Ok(value)
            }
        }
    }

    fn get_client_status_from_table_row(&self, row: &str) -> OperationResult<OpenVpnClient> {
        debug!("get client status from row '{row}'");
        let parts = row.split(",").collect::<Vec<&str>>();

        if parts.len() == 5 {
            Ok(
                OpenVpnClient {
                    name: parts.first().unwrap().to_string(),
                    real_address: parts.get(1).unwrap().to_string(),
                    bytes_received: parts.get(2).unwrap().parse()?,
                    bytes_sent: parts.get(3).unwrap().parse()?,
                    connected_since: self.parse_datetime_from_status_string(
                        &parts.last().unwrap().to_string())?,
                    virtual_address: "".to_string(),
                    last_ref: Local::now().naive_local()
                }
            )

        } else if parts.len() == 4 {
            Ok(
                OpenVpnClient {
                    name: parts.get(1).unwrap().to_string(),
                    real_address: parts.get(2).unwrap().to_string(),
                    bytes_received: 0,
                    bytes_sent: 0,
                    last_ref: self.parse_datetime_from_status_string(
                        &parts.last().unwrap().to_string())?,
                    virtual_address: parts.first().unwrap().to_string(),
                    connected_since: Local::now().naive_local()
                }
            )

        } else {
            Err(anyhow!("unsupported client status format"))
        }
    }
}

#[derive(Clone)]
pub enum StatusFileFormat {
    Default,
    Legacy
}

pub fn get_status_file_format(status_file_path: &str) -> OperationResult<StatusFileFormat> {
    let content = fs::read_to_string(status_file_path)?;

    let rows = content.split("\n").collect::<Vec<&str>>();

    if !rows.is_empty() {
        let first_row = rows.first().unwrap();

        if first_row.starts_with("TITLE") {
            Ok(StatusFileFormat::Default)

        } else if first_row.starts_with("OpenVPN CLIENT") {
            Ok(StatusFileFormat::Legacy)

        } else {
            error!("unable to detect status file format version by first row: '{first_row}'");
            Err(anyhow!("unable to detect status file format"))
        }

    } else {
        error!("unable to detect status file format, file is empty");
        Err(anyhow!("unable to detect status file format"))
    }
}

#[cfg(test)]
mod default_format_tests {
    use std::path::Path;

    use chrono::NaiveDateTime;

    use crate::openvpn::status::{OpenVpnClient, OpenVpnClientStatus};
    use crate::openvpn::status::parser::{DEFAULT_DATE_FORMAT, OpenVpnStatusFileParser, StatusFileFormat};

    #[test]
    fn status_should_be_returned() {
        let status_file_path = Path::new("test-data").join("openvpn-status-new.log");

        let parser = OpenVpnStatusFileParser::new(StatusFileFormat::Default);
        let status = parser.get_status(&status_file_path.to_string_lossy().to_string()).unwrap();

        let expected_status = OpenVpnClientStatus {
            openvpn_version: Some("2.4.12".to_string()),
            updated: NaiveDateTime::parse_from_str("Sat Oct  7 08:59:51 2023", DEFAULT_DATE_FORMAT).unwrap(),
            clients: vec![
                OpenVpnClient {
                    name: "eugene-mobile".to_string(),
                    real_address: "91.151.136.211:6038".to_string(),
                    bytes_received: 1450706,
                    bytes_sent: 82717990,
                    connected_since: NaiveDateTime::parse_from_str("Sat Oct  7 08:54:12 2023", DEFAULT_DATE_FORMAT).unwrap(),
                    last_ref: Default::default(),
                    virtual_address: "10.7.0.10".to_string()
                }
            ],
        };

        assert_eq!(status, expected_status);
    }
}

#[cfg(test)]
mod legacy_format_tests {
    use std::path::Path;

    use chrono::NaiveDateTime;

    use crate::openvpn::status::OpenVpnClient;
    use crate::openvpn::status::parser::{LEGACY_DATE_FORMAT, OpenVpnStatusFileParser, StatusFileFormat};

    #[test]
    fn status_should_be_returned() {
        let status_file_path = Path::new("test-data").join("openvpn-status.log");

        let parser = OpenVpnStatusFileParser::new(StatusFileFormat::Legacy);
        let status = parser.get_status(&status_file_path.to_string_lossy().to_string()).unwrap();

        let client = status.clients.iter().find(|c|c.name == "e.hlopkova").unwrap();

        let expected_client = OpenVpnClient {
            name: "e.hlopkova".to_string(),
            real_address: "157.66.159.277:49212".to_string(),
            bytes_received: 378283,
            bytes_sent: 910367,
            connected_since: NaiveDateTime::parse_from_str("Fri Jul 24 19:02:57 2020", LEGACY_DATE_FORMAT).unwrap(),
            last_ref: NaiveDateTime::parse_from_str("Fri Jul 24 19:16:18 2020", LEGACY_DATE_FORMAT).unwrap(),
            virtual_address: "10.2.0.3".to_string()
        };

        assert_eq!(client, &expected_client);
    }
}