use std::process::Command;

use anyhow::{anyhow, Context};
use log::{debug, error, info};
use serde::Serialize;

use crate::types::{EmptyResult, OperationResult};

pub fn start_openvpn_service(service_name: &str) -> EmptyResult {
    info!("starting openvpn service '{service_name}'..");
    send_openvpn_service_command(&service_name, "start")?;
    info!("openvpn service '{service_name}' has been started");
    Ok(())
}

pub fn stop_openvpn_service(service_name: &str) -> EmptyResult {
    info!("stopping openvpn service '{service_name}'..");
    send_openvpn_service_command(&service_name, "stop")?;
    info!("openvpn service '{service_name}' has been stopped");
    Ok(())
}

#[derive(Serialize,PartialEq,Debug,Clone)]
pub enum OpenVpnServiceStatus {
    Running,
    Stopped
}

pub fn get_openvpn_service_status(service_name: &str) -> OperationResult<OpenVpnServiceStatus> {
    info!("getting service '{service_name}' status..");
    let service_status_stdout = send_openvpn_service_command(&service_name, "status")
                                                .context("unable to get openvpn service status")?;

    if service_status_stdout.contains("Active: active (running)") {
        info!("service status: running");
        Ok(OpenVpnServiceStatus::Running)

    } else {
        info!("service status: stopped");
        Ok(OpenVpnServiceStatus::Stopped)
    }
}

fn send_openvpn_service_command(service_name: &str, command: &str) -> OperationResult<String> {
    let args_row = format!("{command} {service_name}");
    let args: Vec<&str> = args_row.split(" ").collect();

    let output = Command::new("systemctl").args(args).output()?;

    if output.status.success() {
        let stdout = format!("{}", String::from_utf8_lossy(&output.stdout));

        debug!("<stdout>");
        debug!("{}", stdout);
        debug!("</stdout>");

        Ok(stdout)

    } else {
        error!("command execution error");
        let stderr = String::from_utf8_lossy(&output.stderr);

        error!("<stderr>");
        error!("{}", stderr);
        error!("</stderr>");

        Err(anyhow!("service command execution error"))
    }
}