use std::fs;

use log::{debug, info};
use sqlx::{Pool, Sqlite};

use crate::config::OpenVpnConfig;
use crate::openvpn::client::config::{get_ovpn_config, get_ovpn_file_path};
use crate::openvpn::user::UserStatus;
use crate::state::user::find::find_users;
use crate::types::EmptyResult;

pub async fn rebuild_ovpn_configs(db_pool: &Pool<Sqlite>, openvpn_config: &OpenVpnConfig) -> EmptyResult {
    info!("rebuild ovpn configs for users..");
    let users = find_users(&db_pool).await?;

    for user in users.iter() {
        if user.status == UserStatus::Enabled {
            let ovpn_config = get_ovpn_config(&db_pool, &openvpn_config, &user.name).await?.unwrap();
            fs::write(get_ovpn_file_path(&user.name), ovpn_config)?;
            debug!("- user '{}' - ok", &user.name);
        }
    }

    info!("ovpn configs have been rebuilt");

    Ok(())
}