use std::path::{Path, PathBuf};

use anyhow::Context;
use log::{debug, info};
use sqlx::{Pool, Sqlite};
use tera::Tera;
use tokio::fs;

use crate::config::OpenVpnConfig;
use crate::openvpn::easyrsa::get_easyrsa_pki_path;
use crate::state::user::find::find_user_by_name;
use crate::types::{OperationResult, OptionalResult};

pub fn get_ovpn_file_path(username: &str) -> PathBuf {
    Path::new("ovpn").join(format!("{username}.ovpn"))
}

pub async fn get_ovpn_config(db_pool: &Pool<Sqlite>, openvpn_config: &OpenVpnConfig,
                       username: &str) -> OptionalResult<String> {

    let user = find_user_by_name(&db_pool, username).await?;

    match user {
        Some(_) => {
            let ovpn_template_file = Path::new("ovpn.template");

            let ovpn_template = fs::read_to_string(ovpn_template_file).await?;

            let port = format!("{}", &openvpn_config.public_port);

            let ca_file_path = get_easyrsa_pki_path(&openvpn_config.easy_rsa_path)
                .join("ca.crt");
            let ca_content = fs::read_to_string(ca_file_path).await.context("ca.crt read error")?;
            let ca_content = extract_certificate_from_content(&ca_content);

            let client_cert_file_path = get_easyrsa_pki_path(&openvpn_config.easy_rsa_path)
                .join("issued").join(format!("{username}.crt"));
            debug!("client cert file path '{}'", client_cert_file_path.display());

            let client_cert_content = fs::read_to_string(client_cert_file_path).await.context("user cert read error")?;
            let client_cert_content = extract_certificate_from_content(&client_cert_content);

            let client_private_key_file_path = get_easyrsa_pki_path(&openvpn_config.easy_rsa_path)
                .join("private").join(format!("{username}.key"));

            debug!("client private key file path '{}'", client_private_key_file_path.display());

            let client_private_key_content = fs::read_to_string(client_private_key_file_path).await?;
            let client_private_key_content = extract_certificate_from_content(&client_private_key_content);

            let mut tls_auth_content = String::new();

            if !openvpn_config.tls_auth_key_path.is_empty() {
                debug!("ta.key file path '{}'", &openvpn_config.tls_auth_key_path);

                tls_auth_content = fs::read_to_string(&openvpn_config.tls_auth_key_path).await?;
                tls_auth_content = extract_certificate_from_content(&tls_auth_content);
            }

            let ovpn_content = render_ovpn_config(
                &ovpn_template, &openvpn_config.public_host, &port,
                &openvpn_config.protocol_type, &ca_content, &client_cert_content,
                &client_private_key_content, &tls_auth_content
            )?;

            info!("ovpn configuration has been generated");

            Ok(Some(ovpn_content))
        }
        None => {
            info!("user '{username}' wasn't found");
            Ok(None)
        }
    }
}

fn extract_certificate_from_content(input: &str) -> String {
    let rows = input.split("\n").collect::<Vec<&str>>();

    let mut result: Vec<String> = vec![];

    let mut start_found = false;

    for row in rows.iter() {
        let sanitized = row.trim_end_matches('\n').trim().to_string();

        if sanitized.starts_with("#") || sanitized.is_empty() {
            continue
        }

        if sanitized.starts_with("-----END") {
            result.push(sanitized.to_string());
            break;
        }

        if start_found {
            result.push(sanitized.to_string());
        }

        if sanitized.starts_with("-----BEGIN") {
            start_found = true;
            result.push(sanitized.to_string());
        }
    }

    result.join("\n").trim_end_matches('\n').to_string()
}

pub fn render_ovpn_config(template: &str, server_host: &str, server_port: &str, network_protocol: &str,
                          ca_cert_content: &str, client_cert_content: &str,
                          client_private_key_content: &str,
                          tls_auth_key_content: &str) ->OperationResult<String> {

    let mut tera = Tera::default();

    let mut context = tera::Context::new();
    context.insert("server_host", server_host);
    context.insert("server_port", server_port);
    context.insert("proto", network_protocol);
    context.insert("ca_cert", ca_cert_content);
    context.insert("client_cert", client_cert_content);
    context.insert("private_key", client_private_key_content);
    context.insert("tls_auth_key", tls_auth_key_content);

    tera.add_raw_template("ovpn", template)?;

    let content = tera.render("ovpn", &context)?;

    Ok(content)
}

#[cfg(test)]
mod tests {
    use std::path::Path;

    use tokio::fs;

    use crate::openvpn::client::config::extract_certificate_from_content;

    #[tokio::test]
    async fn ca_cert_should_be_returned() {
        check_cert_extraction("user.crt", "user.crt-expected").await;
    }

    #[tokio::test]
    async fn tls_auth_key_should_be_returned() {
        check_cert_extraction("ta.key", "ta.key-expected").await;
    }

    async fn check_cert_extraction(src_file: &str, expected_file: &str) {
        let source_file_path = Path::new("test-data").join(src_file);
        let expected_file_path = Path::new("test-data").join(expected_file);

        let source_content = fs::read_to_string(&source_file_path).await.unwrap();
        let expected_content = fs::read_to_string(&expected_file_path).await.unwrap();

        let result = extract_certificate_from_content(&source_content);

        assert_eq!(expected_content, result);
    }
}