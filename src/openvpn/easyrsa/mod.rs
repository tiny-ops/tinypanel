// easyrsa build-client-full $userName nopass

use std::path::{Path, PathBuf};
use std::process::Command;

use anyhow::anyhow;
use log::{debug, error, info};

use crate::types::{EmptyResult, OperationResult};

pub fn get_certificate_subject(username: &str) -> String {
    format!("/CN={username}")
}

pub fn get_easyrsa_pki_path(easy_rsa_path: &str) -> PathBuf {
    Path::new(easy_rsa_path).join("pki")
}

pub fn get_easyrsa_registry_path(easy_rsa_path: &str) -> PathBuf {
    get_easyrsa_pki_path(easy_rsa_path).join("index.txt")
}

pub fn create_user_certificate(easy_rsa_path: &str, username: &str) -> EmptyResult {
    info!("create user certificate for '{username}'");
    let command = format!("build-client-full {username} nopass");
    send_easyrsa_command(easy_rsa_path, &command)?;
    Ok(())
}

pub fn revoke_user_certificate(easy_rsa_path: &str, username: &str) -> EmptyResult {
    info!("revoke user certificate for '{username}'");
    let command = format!("revoke {username}");
    send_easyrsa_command(easy_rsa_path, &command)?;
    Ok(())
}

fn send_easyrsa_command(easy_rsa_path: &str, command: &str) -> OperationResult<String> {
    debug!("easy-rsa command '{command}'");

    let args_row = format!("easyrsa {command}");
    let args: Vec<&str> = args_row.split(" ").collect();

    let output = Command::new("bash").args(args)
                                .current_dir(easy_rsa_path).output()?;

    if output.status.success() {
        let stdout = format!("{}", String::from_utf8_lossy(&output.stdout));

        debug!("<stdout>");
        debug!("{}", stdout);
        debug!("</stdout>");

        Ok(stdout)

    } else {
        error!("command execution error");
        let stderr = String::from_utf8_lossy(&output.stderr);

        error!("<stderr>");
        error!("{}", stderr);
        error!("</stderr>");

        Err(anyhow!("easyrsa command execution error"))
    }
}

#[cfg(test)]
mod tests {
    use easy_rsa_registry::EasyRsaCertificateStatus;
    use easy_rsa_registry::read::read_certs_from_file;

    use crate::openvpn::easyrsa::{create_user_certificate, revoke_user_certificate};
    use crate::tests::{get_random_string, init_logging};
    use crate::tests::easyrsa::{get_easyrsa_path, get_easyrsa_pki_path};

    #[test]
    fn certs_should_be_created() {
        init_logging();

        let easyrsa_path = get_easyrsa_path();
        let easyrsa_path = format!("{}", easyrsa_path.display());

        let username = get_random_string();

        create_user_certificate(&easyrsa_path, &username).unwrap();

        let pki_path = get_easyrsa_pki_path();
        let cert_file_path = pki_path.join("issued").join(format!("{username}.crt"));
        assert!(cert_file_path.exists());

        let private_key_file_path = pki_path.join("private").join(format!("{username}.key"));
        assert!(private_key_file_path.exists());
    }

    #[test]
    fn cert_should_be_revoked() {
        init_logging();

        let easyrsa_path = get_easyrsa_path();
        let easyrsa_path = format!("{}", easyrsa_path.display());

        let username = get_random_string();

        create_user_certificate(&easyrsa_path, &username).unwrap();
        revoke_user_certificate(&easyrsa_path, &username).unwrap();

        let registry_path = get_easyrsa_pki_path().join("index.txt");
        let registry_path = format!("{}", registry_path.display());

        let certs = read_certs_from_file(&registry_path).unwrap();

        let cert = certs.iter()
            .find(|c| c.certificate_subject.contains(&username)).unwrap();

        assert_eq!(cert.status, EasyRsaCertificateStatus::Revoked);
    }
}