use std::fs;

use anyhow::anyhow;
use easy_rsa_registry::EasyRsaCertificateStatus;
use easy_rsa_registry::read::read_certs_from_file;
use log::{error, info, warn};
use sqlx::{Pool, Sqlite};

use crate::config::OpenVpnConfig;
use crate::openvpn::client::config::{get_ovpn_config, get_ovpn_file_path};
use crate::openvpn::easyrsa::get_easyrsa_pki_path;
use crate::openvpn::user::{User, UserStatus};
use crate::state::user::find::find_user_by_name;
use crate::state::user::save::save_user;
use crate::types::EmptyResult;

pub async fn import_users_from_easy_rsa_registry(db_pool: &Pool<Sqlite>,
                                                 openvpn_config: &OpenVpnConfig) -> EmptyResult {
    info!("import users from easy-rsa registry, path '{}'..", &openvpn_config.easy_rsa_path);

    let registry_file_path = get_easyrsa_pki_path(&openvpn_config.easy_rsa_path).join("index.txt");
    let registry_file_path = format!("{}", registry_file_path.display());

    let certs = read_certs_from_file(&registry_file_path)?;

    let mut has_errors = false;

    for cert in certs.iter() {
        let parts = &cert.certificate_subject.split("=").collect::<Vec<&str>>();

        if parts.len() == 2 {
            let username = parts.last().unwrap();

            let cert_file_path = get_easyrsa_pki_path(&openvpn_config.easy_rsa_path)
                                        .join("issued").join(format!("{username}.crt"));
            let private_key_file_path = get_easyrsa_pki_path(&openvpn_config.easy_rsa_path)
                                        .join("private").join(format!("{username}.key"));

            if cert_file_path.exists() && private_key_file_path.exists() {
                let user = find_user_by_name(&db_pool, &username).await?;

                let status = match cert.status {
                    EasyRsaCertificateStatus::Valid => UserStatus::Enabled,
                    _ => UserStatus::Disabled,
                };

                if user.is_none() {
                    let user = User {
                        id: 0,
                        name: username.to_string(),
                        full_name: username.to_string(),
                        cert_serial: cert.serial_number.to_string(),
                        status: status.clone()
                    };

                    save_user(&db_pool, &user).await?;

                    if status == UserStatus::Enabled {
                        let ovpn_config = get_ovpn_config(
                            &db_pool, &openvpn_config, username).await?.unwrap();

                        fs::write(get_ovpn_file_path(username), ovpn_config)?;
                    }

                    info!("user '{username}' import - ok");
                }

            } else {
                warn!("user '{username}' doesn't have certificate or/and private key, check pki/issued and pki/private paths manually")
            }

        } else {
            error!("import error, unsupported common name format '{}', interrupt import", cert.certificate_subject);
            has_errors = true;
            break;
        }
    }

    if !has_errors {
        info!("users import complete");
        Ok(())

    } else {
        Err(anyhow!("import error"))
    }
}