use std::fs;

use anyhow::anyhow;
use easy_rsa_registry::read::read_certs_from_file;
use log::{debug, error, info};
use sqlx::{Pool, Sqlite};

use crate::config::OpenVpnConfig;
use crate::openvpn::client::config::{get_ovpn_config, get_ovpn_file_path};
use crate::openvpn::easyrsa::{create_user_certificate, get_certificate_subject, get_easyrsa_registry_path};
use crate::openvpn::user::{User, UserStatus};
use crate::state::user::find::find_user_by_name;
use crate::state::user::save::save_user;
use crate::types::EmptyResult;

/// # Create openvpn user
///
/// **Steps:**
/// 1. Generates certificates with easy-rsa
/// 2. Creates user entity in app database
pub async fn create_openvpn_user(db_pool: &Pool<Sqlite>,
                 openvpn_config: &OpenVpnConfig, username: &str, full_name: &str) -> EmptyResult {
    info!("creating openvpn user '{username}'..");
    debug!("easy-rsa path '{}'..", &openvpn_config.easy_rsa_path);

    let existing_user = find_user_by_name(&db_pool, &username).await?;

    if existing_user.is_none() {
        match create_user_certificate(&openvpn_config.easy_rsa_path, username) {
            Ok(_) => {

                let cert_registry = get_easyrsa_registry_path(&openvpn_config.easy_rsa_path);
                let cert_registry = format!("{}", cert_registry.display());

                let certs = read_certs_from_file(&cert_registry)?;
                let cert_subject = get_certificate_subject(username);

                let cert = certs.iter().find(|c|c.certificate_subject == cert_subject);

                match cert {
                    Some(cert) => {
                        let user = User {
                            id: 0,
                            name: username.to_string(),
                            full_name: full_name.to_string(),
                            cert_serial: cert.serial_number.to_string(),
                            status: UserStatus::Enabled,
                        };

                        save_user(&db_pool, &user).await?;

                        let ovpn_content = get_ovpn_config(&db_pool, &openvpn_config, username).await?.unwrap();

                        fs::write(get_ovpn_file_path(username), ovpn_content)?;

                        info!("openvpn user '{username}' has been created");

                        Ok(())
                    }
                    None => {
                        error!("unexpected error, certificate wasn't found in registry by subject '{username}'");
                        Err(anyhow!("unexpected error, certificate wasn't found in registry by subject"))
                    }
                }
            }
            Err(e) => {
                error!("unable to create openvpn user '{username}': {}", e);
                Err(anyhow!("unable to generate openvpn certs for user"))
            }
        }

    } else {
        error!("unable to create openvpn user '{username}': already exists");
        Err(anyhow!("user already exists"))
    }
}

#[cfg(test)]
mod tests {
    use std::str::FromStr;

    use non_blank_string_rs::NonBlankString;
    use non_blank_string_rs::utils::get_random_nonblank_string;

    use crate::config::OpenVpnConfig;
    use crate::openvpn::user::create::create_openvpn_user;
    use crate::state::user::save::save_user;
    use crate::tests::{init_logging, prepare_test_memory_db};
    use crate::tests::easyrsa::get_easyrsa_path;
    use crate::tests::user::get_sample_user;

    #[tokio::test]
    async fn return_error_for_existing_user() {
        init_logging();

        let db_pool = prepare_test_memory_db().await;

        let user = get_sample_user();

        save_user(&db_pool, &user).await.unwrap();

        let easy_rsa_path = get_easyrsa_path();
        let easy_rsa_path = format!("{}", easy_rsa_path.display());

        let config = OpenVpnConfig {
            public_host: get_random_nonblank_string(),
            public_port: 0,
            service_name: get_random_nonblank_string(),
            protocol_type: get_random_nonblank_string(),
            easy_rsa_path: NonBlankString::from_str(easy_rsa_path.as_str()).unwrap(),
            tls_auth_key_path: "".to_string(),
            status_file_path: get_random_nonblank_string(),
        };

        assert!(create_openvpn_user(&db_pool, &config,
                           &user.name, &user.full_name).await.is_err())
    }
}