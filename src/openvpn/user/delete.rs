use easy_rsa_registry::read::read_certs_from_file;
use easy_rsa_registry::remove::remove_cert;
use log::info;
use sqlx::{Pool, Sqlite};

use crate::state::user::find::find_user_by_name;
use crate::state::user::remove::remove_user;
use crate::types::EmptyResult;

pub async fn delete_openvpn_user(db_pool: &Pool<Sqlite>,
                                 easy_rsa_path: &str, username: &str) -> EmptyResult {
    info!("deleting openvpn user '{username}'..");

    let user = find_user_by_name(&db_pool, username).await?;

    match user {
        Some(user) => {
            let certs = read_certs_from_file(&easy_rsa_path)?;

            let cert = certs.iter()
                          .find(|c|c.serial_number == user.cert_serial);

            match cert {
                Some(cert) =>
                            remove_cert(&easy_rsa_path, &cert.serial_number)?,
                None => {}
            }

            remove_user(&db_pool, &username).await?;

            Ok(())
        }
        None => {
            info!("user wasn't found by name '{username}', skip");
            Ok(())
        }
    }
}