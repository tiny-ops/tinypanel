use serde::Serialize;

pub mod create;
pub mod disable;
pub mod enable;
pub mod import;
pub mod delete;

#[derive(Serialize, Clone, PartialEq, Debug)]
#[serde(rename_all = "camelCase")]
pub struct User {
    pub id: u32,
    pub name: String,
    pub full_name: String,
    pub cert_serial: String,
    pub status: UserStatus
}

#[derive(Serialize, Clone, PartialEq, Debug)]
pub enum UserStatus {
    Enabled,
    Disabled
}