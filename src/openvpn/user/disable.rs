use easy_rsa_registry::EasyRsaCertificateStatus;
use easy_rsa_registry::read::read_certs_from_file;
use easy_rsa_registry::update::update_cert_status;
use log::{debug, info};
use sqlx::{Pool, Sqlite};

use crate::openvpn::easyrsa::{get_certificate_subject, get_easyrsa_registry_path};
use crate::openvpn::user::UserStatus;
use crate::state::user::find::find_user_by_name;
use crate::state::user::save::save_user;
use crate::types::EmptyResult;

/// # Disable OpenVPN user
pub async fn disable_openvpn_user(db_pool: &Pool<Sqlite>,
                                 easy_rsa_path: &str, username: &str) -> EmptyResult {
    info!("disable openvpn user '{username}'..");
    debug!("easy-rsa path '{easy_rsa_path}'..");

    let existing_user = find_user_by_name(&db_pool, &username).await?;

    match existing_user {
        Some(user) => {
            let registry_file_path = get_easyrsa_registry_path(&easy_rsa_path);
            let registry_file_path = format!("{}", registry_file_path.display());

            let certs = read_certs_from_file(&registry_file_path)?;

            let certificate_subject = get_certificate_subject(username);

            let cert_found = certs.iter()
                .find(|c| c.serial_number == user.cert_serial);

            match cert_found {
                Some(cert) => {
                    update_cert_status(&registry_file_path,
                                       &cert.serial_number, EasyRsaCertificateStatus::Revoked)?;
                    info!("certificate status '{certificate_subject}' has been updated to revoked");
                }
                None => info!("certificate wasn't found by name '{certificate_subject}' in easy-rsa registry, skip")
            }

            let mut updated_user = user.clone();
            updated_user.status = UserStatus::Disabled;
            save_user(&db_pool, &updated_user).await?;
            info!("user '{username}' has been disabled");
        },
        None => info!("user wasn't found in db '{username}', skip")
    }

    Ok(())
}