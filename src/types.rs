pub type OperationResult<T> = anyhow::Result<T>;
pub type OptionalResult<T> = anyhow::Result<Option<T>>;
pub type EmptyResult = anyhow::Result<()>;