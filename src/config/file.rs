use std::path::Path;

use config::Config;
use log::info;

use crate::config::AppConfig;
use crate::types::OperationResult;

pub fn loading_config_from_file(config_file: &Path) -> OperationResult<AppConfig> {
    info!("loading config from files: '{}'", config_file.display());

    let config_file_str = format!("{}", config_file.display());

    let settings = Config::builder()
        .add_source(config::File::with_name(&config_file_str))
        .build()
        .unwrap();

    let config = settings.try_deserialize::<AppConfig>()?;

    info!("config:");
    info!("{:?}", config);

    Ok(config)
}