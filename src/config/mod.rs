use non_blank_string_rs::NonBlankString;
use serde::{Deserialize, Serialize};

pub mod file;

#[derive(PartialEq, Deserialize, Clone, Debug)]
#[serde(rename_all = "kebab-case")]
pub struct AppConfig {
    pub port: u16,

    pub log_level: NonBlankString,

    pub db_cnn: NonBlankString,

    pub openvpn: OpenVpnConfig
}

#[derive(PartialEq, Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct OpenVpnConfig {
    #[serde(alias = "public-host")]
    pub public_host: NonBlankString,

    #[serde(alias = "public-port")]
    pub public_port: u16,

    #[serde(alias = "service-name")]
    pub service_name: NonBlankString,

    #[serde(alias = "protocol-type")]
    pub protocol_type: NonBlankString,

    #[serde(alias = "easy-rsa-path")]
    pub easy_rsa_path: NonBlankString,

    #[serde(alias = "tls-auth-key-path")]
    pub tls_auth_key_path: String,

    #[serde(alias = "status-file-path")]
    pub status_file_path: NonBlankString,
}