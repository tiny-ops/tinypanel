use std::path::Path;

use anyhow::Context;

use tinypanel::config::file::loading_config_from_file;
use tinypanel::startup::Application;
use tinypanel::types::EmptyResult;

#[tokio::main]
async fn main() -> EmptyResult {
    let config_file = Path::new("config.yml");

    let config = loading_config_from_file(&config_file)
        .context("couldn't load config from file")?;

    let app = Application::build(config).await?;

    app.run_until_stopped().await?;

    Ok(())
}
