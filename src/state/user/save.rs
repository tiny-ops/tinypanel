use log::{error, info};
use sqlx::{Error, Pool, Sqlite};
use sqlx::Error::RowNotFound;

use crate::openvpn::user::User;
use crate::state::user::find::find_user_by_name;
use crate::state::user::get_user_status_code;

pub async fn save_user(db_pool: &Pool<Sqlite>, user: &User) -> Result<User, Error> {
    info!("saving user '{}'..", user.name);

    let existing_user = find_user_by_name(&db_pool, &user.name).await?;

    let status = get_user_status_code(&user.status);

    match existing_user {
        Some(existing_user) => {
            sqlx::query(r#"UPDATE users SET full_name=$1, status=$2 WHERE name=$3;"#)
                .bind(&existing_user.full_name)
                .bind(status)
                .bind(existing_user.name)
                .execute(db_pool)
                .await?;

            info!("user '{}' has been updated", user.name);
        }
        None => {
            sqlx::query(r#"INSERT INTO users (name, full_name, cert_serial, status) VALUES ($1, $2, $3, $4);"#)
                .bind(&user.name)
                .bind(&user.full_name)
                .bind(&user.cert_serial)
                .bind(status)
                .execute(db_pool)
                .await?;

            info!("user '{}' has been saved", user.name)
        }
    }

    let existing_user = find_user_by_name(&db_pool, &user.name).await?;

    match existing_user {
        Some(user) => {
            Ok(user)
        }
        None => {
            error!("unexpected error, row not found");
            Err(RowNotFound)
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::openvpn::user::UserStatus;
    use crate::state::user::find::find_users;
    use crate::state::user::save::save_user;
    use crate::tests::{get_random_string, init_logging, prepare_test_memory_db};
    use crate::tests::user::get_sample_user;

    #[tokio::test]
    async fn name_should_not_be_updated() {
        init_logging();

        let db_pool = prepare_test_memory_db().await;

        let user = get_sample_user();

        let mut saved_user = save_user(&db_pool, &user).await.unwrap();
        let new_username = get_random_string();
        saved_user.name = new_username.to_string();

        save_user(&db_pool, &saved_user).await.unwrap();

        let users = find_users(&db_pool).await.unwrap();

        assert_eq!(2, users.len());

        assert!(users.iter().find(|u|u.name == user.name).is_some());
        assert!(users.iter().find(|u|u.name == new_username).is_some());
    }

    #[tokio::test]
    async fn properties_should_be_updated() {
        init_logging();

        let db_pool = prepare_test_memory_db().await;

        let mut user = get_sample_user();
        user.full_name = get_random_string();
        user.status = UserStatus::Disabled;

        let saved_user = save_user(&db_pool, &user).await.unwrap();

        let mut updated_user = saved_user.clone();
        updated_user.full_name = get_random_string();
        updated_user.status = UserStatus::Enabled;

        let result_user = save_user(&db_pool, &updated_user).await.unwrap();

        let users = find_users(&db_pool).await.unwrap();

        assert_eq!(1, users.len());

        let result = users.iter().find(|u| u.name == user.name).unwrap();

        assert_eq!(result_user, result.clone());
    }
}