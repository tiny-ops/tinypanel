use crate::openvpn::user::UserStatus;

pub mod save;
pub mod find;
pub mod remove;

pub fn get_user_status_code(status: &UserStatus) -> u8 {
    match status {
        UserStatus::Enabled => 0,
        UserStatus::Disabled => 1
    }
}

pub fn get_user_status_from_code(status_code: u8) -> UserStatus {
    match status_code {
        0 => UserStatus::Enabled,
        _ => UserStatus::Disabled
    }
}