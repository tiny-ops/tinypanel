use log::info;
use sqlx::{Error, Pool, Sqlite};

pub async fn remove_user(db_pool: &Pool<Sqlite>, name: &str) -> Result<(), Error> {
    info!("removing user '{name}'..");

    sqlx::query(r#"DELETE FROM users WHERE name=$1;"#)
        .bind(&name)
        .execute(db_pool)
        .await?;

    info!("user '{name}' has been removed");

    Ok(())
}

#[cfg(test)]
mod tests {
    use crate::state::user::find::find_users;
    use crate::state::user::remove::remove_user;
    use crate::state::user::save::save_user;
    use crate::tests::prepare_test_memory_db;
    use crate::tests::user::get_sample_user;

    #[tokio::test]
    async fn user_should_be_removed() {
        let db_pool = prepare_test_memory_db().await;

        let user = get_sample_user();

        save_user(&db_pool, &user).await.unwrap();

        let users = find_users(&db_pool).await.unwrap();

        assert!(!users.is_empty());

        remove_user(&db_pool, &user.name).await.unwrap();

        let users = find_users(&db_pool).await.unwrap();

        assert!(users.is_empty());
    }
}