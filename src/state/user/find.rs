use sqlx::{Error, Pool, Row, Sqlite};
use sqlx::sqlite::SqliteRow;

use crate::openvpn::user::User;
use crate::state::user::get_user_status_from_code;

pub async fn find_users(db_pool: &Pool<Sqlite>) -> Result<Vec<User>, Error> {
    let select_query = sqlx::query("SELECT * FROM users ORDER BY name");

    let users: Vec<User> = select_query
        .map(|row: SqliteRow| {
            let status: u8 = row.get("status");
            let status = get_user_status_from_code(status);

            User {
                id: row.get("id"),
                name: row.get("name"),
                full_name: row.get("full_name"),
                cert_serial: row.get("cert_serial"),
                status,
            }
        })
        .fetch_all(db_pool)
        .await?;

    Ok(users)
}

pub async fn find_user_by_name(db_pool: &Pool<Sqlite>, name: &str) -> Result<Option<User>, Error> {
    let users = find_users(&db_pool).await?;
    let user = users.into_iter().find(|u| u.name == name);
    Ok(user)
}