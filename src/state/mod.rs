use log::info;
use sqlx::{Error, Pool, Sqlite};
use sqlx::sqlite::SqlitePoolOptions;

pub mod user;

/// Подключение к базе данных состояния приложения
/// Например, `sqlite://app-factory.db?mode=rwc` или `sqlite::memory:`
pub async fn connect_to_db(db_cnn: &str) -> Result<Pool<Sqlite>, Error> {
    info!("connecting to db '{db_cnn}'..");
    SqlitePoolOptions::new()
        .max_connections(1)
        .connect(db_cnn)
        .await
}

pub async fn create_db_tables(pool: &Pool<Sqlite>) -> Result<(), Error> {
    info!("creating tables in db..");

    sqlx::query(
        r#"
CREATE TABLE IF NOT EXISTS users (
  id integer PRIMARY KEY AUTOINCREMENT,
  name text,
  full_name text,
  cert_serial text,
  status integer,
  UNIQUE(name)
);"#,
    ).execute(pool)
        .await?;

    Ok(())
}