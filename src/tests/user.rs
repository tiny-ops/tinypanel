use crate::openvpn::user::{User, UserStatus};
use crate::tests::get_random_string;

pub fn get_sample_user() -> User {
    User {
        id: 0,
        name: get_random_string(),
        full_name: get_random_string(),
        cert_serial: get_random_string(),
        status: UserStatus::Enabled,
    }
}