use fake::{Fake, Faker};
use log::LevelFilter;
use sqlx::{Pool, Sqlite};

use crate::state::{connect_to_db, create_db_tables};

pub mod user;
pub mod easyrsa;

pub fn init_logging() {
    let _ = env_logger::builder().filter_level(LevelFilter::Debug)
        .is_test(true).try_init();
}

pub fn get_random_string() -> String {
    Faker.fake::<String>()
}

pub async fn prepare_test_memory_db() -> Pool<Sqlite> {
    let pool = connect_to_db("sqlite::memory:").await.unwrap();
    create_db_tables(&pool).await.unwrap();
    pool
}