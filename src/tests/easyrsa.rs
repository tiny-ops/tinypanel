use std::path::{Path, PathBuf};

pub fn get_easyrsa_path() -> PathBuf {
    Path::new("test-data").join("easyrsa")
}

pub fn get_easyrsa_pki_path() -> PathBuf {
    get_easyrsa_path().join("pki")
}