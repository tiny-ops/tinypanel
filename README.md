# Tiny Panel

Dashboard for OpenVPN service.

## Documentation

- Install
- [API](docs/API.md)
- [Security](docs/SECURITY.md)
- FAQ

## Progress for 1.0.0

- [x] Backend
  - [x] Load config from file
  - [x] Get active connections
    - [x] Parse status file
  - [x] RSA Registry File Management
  - [x] EasyRSA Wrapper
    - [x] Create cert 
    - [x] Revoke cert 
    - [x] Un-revoke 
  - [x] User management
    - [x] Create user (with certs) 
    - [x] Endpoint: Create user
    - [x] Disable user (revoke cert)
    - [x] Endpoint: Revoke user cert
    - [x] Enable user (un-revoke cert)
    - [x] Endpoint: Un-revoke user cert
    - [x] Endpoint: Download ovpn config
    - [x] Import existing certs as users
    - [x] Endpoint: Import existing certs as users
  - [x] OpenVPN Service Management
    - [x] Stop
    - [x] Start
    - [x] Status
    - [x] Endpoints

- [ ] Frontend
  - [x] Status Page
    - [x] Show active clients
  - [x] User Management Page
    - [x] Create user
    - [x] Disable user
    - [x] Enable user
    - [x] Download OVPN-file for user
  - [x] OpenVPN Service Management Page
    - [x] Stop
    - [x] Start
  - [x] OVPN Template management Page
    - [x] Show template

## RoadMap

- Sort filters: by incoming traffic, by 'connected since'
- Remove user
- Kick active user
- Superadmin access
- View access
- OVPN Template management from UI